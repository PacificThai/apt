/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 * Course Code      : COSC2408
 * Program Code     : BP162B
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_options.h"

/* Function Prototypes for BOOLEAN */
BOOLEAN removeStock(struct ppd_system * system, char * id);
BOOLEAN changeCheck(struct ppd_system * system, unsigned change);

/**
 * @file ppd_options.c this is where you need to implement the main 
 * options for your program. You may however have the actual work done
 * in functions defined elsewhere. 
 * @note if there is an error you should handle it within the function
 * and not simply return FALSE unless it is a fatal error for the 
 * task at hand. You want people to use your software, afterall, and
 * badly behaving software doesn't get used.
 **/

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this particular function should never fail.
 **/
BOOLEAN display_items(struct ppd_system * system)
{
   char price[PRICE_LEN + 1];
   struct ppd_node *current;
   current = system->item_list->head;
   
   /* Header information */
   printf("\nItems Menu\n");
   printf("-----------\n");
   printf("ID    |Name                |Available |Price\n");
   printf("----------------------------------------------\n");
   
   /* Body information */
   while (current != NULL){
      getPrice(price, current->data->price.dollars, current->data->price.cents);
      printf("%-6s|", current->data->id);
      printf("%-20s|", current->data->name);
      printf("%-10u|", current->data->on_hand);
      printf("%s", price);
      printf("\n");
      current = current->next;
   }
   printf("\n");
   
   return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when a purchase succeeds and false when it does not
 **/
BOOLEAN purchase_item(struct ppd_system * system)
{
   struct ppd_node * item;
   BOOLEAN purchaseDone = FALSE;
   BOOLEAN changeFlag = FALSE;
   int index, inner; 
   char id[IDLEN + 1];
   char prompt[PROMPTLEN + 1];
   char priceString[PRICE_LEN + 1];
   /* Technically the most amount of coins that can inserted is:
    * $99.95 worth of 5c coins = 1999 coins. Assumed 25 is most for now.
    */
   int numDenomInserted = 0;
   unsigned insertedCoins[25];
   char *name, *desc;
   char *abortMsg = "[SYSTEM]: \"Purchase Item\" - aborted, returning to main menu...\n";
   
   unsigned dollars, cents, amount;
   unsigned price, inserted, change;
   
   /* Reference to coin denominations in system */
   unsigned denoms[NUM_DENOMS];
   for (index = 0; index < NUM_DENOMS; index++)
      denoms[index] = system->cash_register[index].denom;
    
   printf("Purchase Item\n");
   printf("-------------\n");
   
   do
   {
      sprintf(prompt, "Please enter the id of the item you wish to purchase: ");
      if(strVerify(id, IDLEN, prompt) == RTM){
         printf("%s", abortMsg);
         return FALSE;
      }
      
      item = searchStock(system, id);
      if(!item)
         printf("[ERROR]: Please enter a valid id. Please try again.\n");
   } while(!item);
   
   amount = item->data->on_hand;
   if(amount == 0){
      printf("[SYSTEM]: The item you wish to purchase is currently out of stock.\n");
      printf("Please try again once it has been restocked.\n");
      printf("returning to main menu...\n");
      return FALSE;
   }
   
   name = item->data->name;
   desc = item->data->desc;    
   dollars = item->data->price.dollars;
   cents = item->data->price.cents;
   
   getPrice(priceString, dollars, cents);
   printf("You have selected \"%s - %s\". This will cost you %s.\n"
         , name, desc, priceString);
            
   printf("Please hand over the money - type in the value of each note/coin in cents.\n");
   printf("Press enter or ctrl-d on a new line to cancel this purchase: \n");
   
   price = (dollars * 100) + cents;
   
   /* Free Item */
   if (price == 0){
      purchaseDone = TRUE;
      printf("[SYSTEM]: You have selected a free product\n");
      printf("Here is your %s\n", name);
   }
   
   /* Loop for transaction, prompting until user paid enough for product */
   while (purchaseDone == FALSE){
      getPrice(priceString, dollars, cents);
      sprintf(prompt, "You still need to give us %s: ", priceString);
      
      /* Ensure correct denomination is entered 
         + check if return to menu requested */
      if(insertCheck(&inserted, PRICELEN, prompt) == RTM){
         printf("%s", abortMsg);
         /* Return coins if at least inserted once */
         if(numDenomInserted > 0){
            printf("Returning you your inserted coins/notes: ");
            for(index=0; index<numDenomInserted; index++){
               /* Removing added cash from register */
               for(inner=NUM_DENOMS; inner >= 0; inner--){
                  if(denoms[inner] == insertedCoins[index])
                     system->cash_register[inner].count--;
               }
            
               /* Returning cash to user: dollars, else cents */
               if(insertedCoins[index] >= 100)
                  printf("$%d ", (insertedCoins[index]/100));
               else
                  printf("%dc ", insertedCoins[index]);
            }
            printf("\n");
         }
         return FALSE;
      }

      /* Add to cash register, price paid, record cash inserted */
      for (index = 0; index < NUM_DENOMS; index++){
         if(denoms[index] == inserted){
            system->cash_register[index].count++;
            insertedCoins[numDenomInserted] = inserted;
            numDenomInserted++;
         }
      }
      
      /* Check if change needs to be paid, else continue */ 
      if(inserted > price)
         purchaseDone = TRUE;
      else {
         price -= inserted;
         convCurrency(price, &dollars, &cents);
      }
      
      /* Check if full price has been paid, no change needed */
      if(price == 0)
         purchaseDone = TRUE;
   }
   
   /* When change needs to be given out */
   if (price != 0){
      change = (inserted - price);
      
      /* Calculating whether possible to give out change */
      changeFlag = changeCheck(system, change);
      
      /* When not enough cash register does not have enough change to give */
      if (changeFlag == FALSE){
         printf("[ERROR]: The cash register does not have enough change to refund\n");
         printf("Returning you your inserted coins/notes: ");
         for(index=0; index<numDenomInserted; index++){
            /* Removing added cash from register */
            for(inner=NUM_DENOMS; inner >= 0; inner--){
               if(denoms[inner] == insertedCoins[index]){
                  system->cash_register[inner].count--;
               }
            }
         
            /* Returning cash to user */
            if(insertedCoins[index] >= 100)
               printf("$%d ", (insertedCoins[index]/100));
            else
               printf("%dc ", insertedCoins[index]);
         }
         printf("\n");
         return FALSE;
      }

      convCurrency(change, &dollars, &cents);
      getPrice(priceString, dollars, cents);

      printf("Thank you here is your %s, and your change of %s ", name, priceString);

      /* Calculating change dollars + cents to give out */
      do{
         for (index = NUM_DENOMS - 1; index >= 0; index--)
         {
            if(denoms[index] <= change && system->cash_register[index].count > 0){
               change -= denoms[index];
               convCurrency(denoms[index], &dollars, &cents);
               /* Remove cash from register as change */
               system->cash_register[index].count--;
               
               /* Print based on whether it was dollars or cents returned */
               if(dollars != 0)
                  printf("$%d ", dollars);
               else
                  printf("%dc ", cents);
               break;
            }
        }
      }while(change != 0);
     
      printf("\n");
   }
   
   printf("Please come again soon.\n");
   item->data->on_hand--;
   
   return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when a save succeeds and false when it does not
 **/
BOOLEAN save_system(struct ppd_system * system)
{
   /* File I/O variables */
   FILE *items_file, *coins_file;
   int i;
   const char *stock_name = system->stock_file_name;
   const char *coin_name = system->coin_file_name;
   
   char on_hand[ON_HAND_LEN + 1];
   char dollars[DOLLARS_LEN + 1];
   char cents[CENTS_LEN + 1];
   char denom[DENOM_LEN + 1];
   char count[COUNT_LEN + 1];
   /* Linked list iteration variables */
   struct ppd_node *previous, *current;
   
   previous = NULL;
   current = system->item_list->head;
   
   items_file = fopen(stock_name, "wb");
   coins_file = fopen(coin_name, "wb");
   
   if (items_file == NULL || coins_file == NULL){
      fprintf(stderr, "[ERROR]: Unable to save files, exiting...\n");
      return FALSE;
   }
   
   /* Writing stock content */
   while (current != NULL) {
      previous = current;
      /* Converting price and on_hand to string format */
      sprintf(on_hand, "%d", current->data->on_hand);
      sprintf(dollars, "%d", current->data->price.dollars);
      sprintf(cents, "%d", current->data->price.cents);
      
      /* ID */
      fwrite(current->data->id, strlen(current->data->id), 1, items_file);
      fwrite("|", sizeof(char), 1, items_file);
      /* Name */
      fwrite(current->data->name, strlen(current->data->name), 1, items_file);
      fwrite("|", sizeof(char), 1, items_file);
      /* Description */
      fwrite(current->data->desc, strlen(current->data->desc), 1, items_file);
      fwrite("|", sizeof(char), 1, items_file);
      /* Price */
      fwrite(dollars, strlen(dollars), 1, items_file);
      fwrite(".", sizeof(char), 1, items_file);
      fwrite(cents, strlen(cents), 1, items_file);
      fwrite("|", sizeof(char), 1, items_file);
      /* On_hand */
      fwrite(on_hand, strlen(on_hand), 1, items_file);
      /* Line separator */
      fwrite("\n", sizeof(char), 1, items_file);
      
      current = current->next;
   }
   
   /* Writing to coins file */
   for (i=NUM_DENOMS-1; i>=0; i--){
      /* Converting denom and quantity to string format */
      sprintf(denom, "%d", system->cash_register[i].denom);
      sprintf(count, "%d", system->cash_register[i].count);
      
      /* Denomination */
      fwrite(denom, strlen(denom), 1, coins_file);
      fwrite(",", sizeof(char), 1, coins_file);      
      /* Count */
      fwrite(count, strlen(count), 1, coins_file);
      fwrite("\n", sizeof(char), 1, coins_file);
   }
   fclose(items_file);
   fclose(coins_file);  

   return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when adding an item succeeds and false when it does not
 **/
BOOLEAN add_item(struct ppd_system * system)
{
   char prompt[PROMPTLEN + 1];
   char name[NAMELEN + 1];
   char desc[DESCLEN + 1];
   char newID[IDLEN + 1];
   char *abortMsg = "[SYSTEM]: \"Add Item\" - aborted, returning to main menu...\n";
   struct price price;

   generateID(system, newID);
   
   /* If a new ID cannot be generated anymore. E.g. latest = I9999 */
   if(strcmp(newID, "[ERROR]") == 0){
      printf("[ERROR]: Cannot add anymore items into the system, returning to main menu...\n");
      return FALSE;
   }

   printf("This new meal item will have the Item id of %s\n", newID);
   
   sprintf(prompt, "Enter the item name: ");
   if(strVerify(name, NAMELEN, prompt) == RTM){
      printf("%s", abortMsg);
      return FALSE;
   }
   
   sprintf(prompt, "Enter the item description: ");
   if(strVerify(desc, DESCLEN, prompt) == RTM){
      printf("%s", abortMsg);
      return FALSE;
   }
   
   sprintf(prompt, "Enter the price for this item: ");
   if(priceCheck(&price, PRICELEN, prompt) == RTM){
      printf("%s", abortMsg);
      return FALSE;
   }
   
   printf("This item \"%s - %s\" has now been added to the menu\n", name, desc);
   
   addStock(system, newID, name, desc, price, DEFAULT_STOCK_LEVEL); 
   return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when removing an item succeeds and false when it does not
 **/
BOOLEAN remove_item(struct ppd_system * system)
{
   struct ppd_node * item;
   char id[IDLEN + 1];
   char prompt[PROMPTLEN + 1];
   char *name, *desc;
   
   sprintf(prompt, "Enter the item id of the item to remove from the menu: ");
   if(strVerify(id, IDLEN, prompt) == RTM){
      printf("[SYSTEM]: \"Remove Item\" - aborted, returning to main menu...\n");
      return FALSE;
   }
   
   item = searchStock(system, id);
   
   /* NULL returned - Item not found */
   if(!item){
      printf("[Error]: Item could not be found, returning to menu...\n");
      return FALSE;
   }
   
   name = item->data->name;
   desc = item->data->desc;    
   printf("\"%s - %s - %s\" has been removed from the system.\n", id, name, desc);
   removeStock(system, id);
   
   return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail.
 **/
BOOLEAN reset_stock(struct ppd_system * system)
{
   struct ppd_node *previous, *current;
   
   previous = NULL;
   current = system->item_list->head;
   
   while (current != NULL) {
      previous = current;
      current->data->on_hand = DEFAULT_STOCK_LEVEL;
      current = current->next;
   }
      
   printf("[SYSTEM]: All stock has been reset to the default level of %d\n"
         , DEFAULT_STOCK_LEVEL);
   return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail.
 **/
BOOLEAN reset_coins(struct ppd_system * system)
{  
   int i;
   for(i=0; i<NUM_DENOMS; i++)
      system->cash_register[i].count = DEFAULT_COIN_COUNT;
      
   printf("[SYSTEM]: All coins has been reset to the default level of %d\n"
         , DEFAULT_COIN_COUNT);
   return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail
 **/
BOOLEAN display_coins(struct ppd_system * system)
{
   int position;
   const char * denominations[NUM_DENOMS] =
   {
      "5 cents",  "10 cents", "20 cents", "50 cents",
      "1 dollar", "2 dollar", "5 dollar", "10 dollar"
   };

   printf("\nCoins Summary\n");
   printf("-------------\n");
   printf("Denomination   | Count\n");
   printf("---------------------\n");
   
   /* Viewing denomination quantity in system */
   for (int position=0; position<NUM_DENOMS; position++){
      printf("%-15s|", denominations[position]);
      printf("    %d\n", system->cash_register[position].count);
   }
   printf("\n");   
   return TRUE;
}