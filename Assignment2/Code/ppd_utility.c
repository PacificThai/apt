/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 * Course Code      : COSC2408
 * Program Code     : BP162B
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_utility.h"
/**
 * @file ppd_utility.c contains implementations of important functions for
 * the system. If you are not sure where to implement a new function, 
 * here is probably a good spot.
 **/

void read_rest_of_line(void)
{
    int ch;
    while(ch = getc(stdin), ch!='\n' && ch != EOF)
        ;
    clearerr(stdin);
}

/**
 * @param system a pointer to a @ref ppd_system struct that contains
 * all the data in the system we are manipulating
 * @param coins_name the name of the coins file
 * @param stock name the name of the stock file
 **/
BOOLEAN load_data(struct ppd_system * system , const char * coins_name, 
const char * stock_name)
{  
   FILE *items_file, *coins_file;
   char itemline[ITEM_LINELEN];
   char coinline[COIN_LINELEN];
   char *token;
   
   /* -1 since enum starts at 0 */
   int index = NUM_DENOMS-1;
   /* Checking if file contains all possible coin denominations */
   int numDenoms = 0;
   char *id, *name, *desc;
   char *priceString, *dollars, *cents, *onhandStr, *denomStr, *qtyStr;
   struct price price;
   unsigned on_hand, quantity, denomination;
   
   items_file = fopen(stock_name, "r");
   coins_file = fopen(coins_name, "r");
   
   if (items_file == NULL || coins_file == NULL){
      fprintf(stderr, 
      "[ERROR]: Make sure to enter the correct files, exiting...\n");
      return FALSE;
   }
   
   /* Checking for empty files */
   fseek(items_file, 0, SEEK_END);
   fseek(coins_file, 0, SEEK_END);
   if(ftell(items_file) == 0 || ftell(coins_file) == 0){
      fprintf(stderr, 
      "[ERROR]: Empty file found, recheck file contents, exiting...\n");
      return FALSE;
   }
   
   /* Return pointer back to start of the file */
   fseek(items_file, 0, SEEK_SET);
   fseek(coins_file, 0, SEEK_SET);
   
   /* Allocation of filenames to system */
   system->stock_file_name = stock_name;
   system->coin_file_name = coins_name;
   
   /* Reading items.dat */
   while (fgets(itemline, sizeof(itemline), items_file) != NULL)
   {
      itemline[sizeof(itemline) - 1] = '\0';
      /* id */
      token = strtok(itemline, "|");
      id = token;
      /* name */
      token = strtok(NULL, "|");
      name = token;
      /* description */
      token = strtok(NULL, "|");
      desc = token;
      /* price */
      token = strtok(NULL, "|");
      priceString = token;
      /* on hand */
      token = strtok(NULL, "|");
      onhandStr = token;
      
      /* File Input field checking */
      /* When there is a/are field(s) missing/blank token */
      if(token == NULL || strcmp(token, "\n") == 0){
         printf("[SYSTEM]: In item file: [%s]\n", stock_name);
         printf("[ERROR]: Field(s) missing: unable to load data, exiting...\n");
         return FALSE;
      }
      /* Ensure correct ID format */
      if(strlen(id) != IDLEN){
         printf("[SYSTEM]: In item file: [%s]\n", stock_name);
         printf("[ERROR]: ID field is not in the correct format, exiting...\n");
         return FALSE;;
      }
      
      /* Obtaining unsigned value for on_hand */
      on_hand = strtoulConv(onhandStr);
      
      /* Converting price into unsigned format */
      dollars = strtok(priceString, ".");
      cents = strtok(NULL, ".");
      
      
      /* Ensure correct price format 
       * 1. Cents is always required
       * 2. Dollars length and cents length each at most == 2 digits
       * 3. Both have to consist of only numerical digits
       */
      if(cents == NULL || 
         strlen(dollars) > DOLLARS_LEN || strlen(cents) > CENTS_LEN ||
         numberCheck(dollars) == FALSE || numberCheck(cents) == FALSE){
         printf("[SYSTEM]: In item file: [%s]\n", stock_name);
         printf("[ERROR]: Price field is not in the correct format, exiting...\n");
         return FALSE;
      }
      /* Assume max number of items == 2 digits (99), check if digits */
      if(strlen(token) > ON_HAND_LEN || numberCheck(onhandStr) == FALSE){
         printf("[SYSTEM]: In item file: [%s]\n", stock_name);
         printf("[ERROR]: On hand field is not in the correct format, exiting...\n");
         return FALSE;
      }
            
      price.dollars = strtoulConv(dollars);
      price.cents = strtoulConv(cents);

      addStock(system, id, name, desc, price, on_hand); 
   }
   
   /* Reading coins.dat */
   while (fgets(coinline, sizeof(coinline), coins_file) != NULL)
   {
      numDenoms++;
      
      coinline[sizeof(coinline) - 1] = '\0';
      /* Denomination */
      token = strtok(coinline, ",");
      denomStr = token;
      denomination = strtoulConv(token);
      /* Ensure length at most is 4. Max = 1000 */
      if(strlen(token) > DENOM_LEN){
         printf("[SYSTEM]: In coin file: [%s]\n", coins_name);
         printf("[ERROR]: Invalid denomination input found, exiting...\n");
         return FALSE;
      }
      /* quantity */
      token = strtok(NULL, ",");
      qtyStr = token;
      /* If unable to tokenize input ',' not found, over 2 digits or blank*/
      if(token == NULL || 
         strlen(token) > COUNT_LEN + 1 || strcmp(token,"\n") == 0)
      {
        printf("[SYSTEM]: In coin file: [%s]\n", coins_name);
        printf("[ERROR]: Invalid data input found, exiting...\n");
        return FALSE;
      }
      quantity = strtoulConv(token);
      
      /* Ensure only digit inputs (non-alphabetical) 
       * strtoulConv will return 0, if input contains alphabetical characters
       * numberCheck should return false if the string contains alphabeticals
       */
      if(numberCheck(denomStr) == FALSE || numberCheck(qtyStr) == FALSE){
         printf("[SYSTEM]: In coin file: [%s]\n", coins_name);
         printf("[ERROR]: Ensure data is only in numerical format, exiting...\n");
         return FALSE;
      }
            
      system->cash_register[index].denom = denomination;
      system->cash_register[index].count = quantity;
      index--;
   }
   
   /* Ensure there are exactly the number of denominations specified in 
    * NUM_DENOMS variable (8)
    */
   if(numDenoms != NUM_DENOMS){
      printf("[SYSTEM]: In coin file: [%s]\n", coins_name);
      printf("[ERROR]: Ensure all possible denominations are present, exiting...\n");
      return FALSE;
   }

   fclose(items_file);
   fclose(coins_file);   

   return TRUE;
}

/**
 * @param system a pointer to a @ref ppd_system struct that holds all 
 * the data for the system we are creating
 **/
BOOLEAN system_init(struct ppd_system * system)
{
   int i;
   /* Default machine denominations, can be overwritten by coin file denoms*/
   unsigned denoms[NUM_DENOMS] = {5, 10, 20, 50, 100, 200, 500, 1000};
   
   /* Allocate memory to item_list */
   system->item_list = malloc(sizeof(struct ppd_list));
   
   /* Initializing cash_register to be empty, no coins */
   for(i=0; i<NUM_DENOMS; i++){
      system->cash_register[i].denom = denoms[i];
      system->cash_register[i].count = 0;
   }
   
   /* Initializing item_list*/
   system->item_list->head = NULL;
   system->item_list->count = 0;
   
   /* Initializing file names */
   system->coin_file_name = NULL;
   system->stock_file_name = NULL;
   
   return TRUE;
}

/**
 * @param system a pointer to a @ref ppd_system struct that holds all 
 * the data for the system we are creating
 **/
void system_free(struct ppd_system * system)
{
   node *current, *next;
   
   current = system->item_list->head;
   while (current != NULL) {
      next = current->next;
      free(current->data);
      free(current);
      current = next;
   }
   
   free(system->item_list);
}

/* Extra function(s) added */
/* Validation of entering a new item into the system */
INPUT priceCheck(struct price * price, unsigned len, char *prompt)
{
   /* dollarsDone = finished checking dollar, move onto checking cents */
   /* digitCheck = check whether string contains any non-digit characters */
   /* dollarFlag = whether there is dollar component or not */
   BOOLEAN exit = FALSE;
   BOOLEAN dollarsDone, digitCheck, dollarFlag;
   char input[len + 2];
   int i, digit, inlen, numDelim;
   unsigned dollars, cents;
   char *token, *userIn;
   
   do
   {
      /* Default values for every iteration of loop */
      dollarsDone = FALSE;
      digitCheck = TRUE;
      dollarFlag = TRUE;
      numDelim = 0;
      
      printf("%s", prompt);
      userIn = fgets(input, len + 2, stdin);
      inlen = strlen(input);

      /* [1] Checking for return to menu entry - [enter] or ctrl-d */
      if (userIn == NULL){
         printf("\n");
         return RTM;
      }
      if (strcmp(userIn, "\n") == 0)
         return RTM;
         
      /* Input too long */
      if (input[inlen - 1] != '\n'){
         printf("[ERROR]: Input was too long\n");
         read_rest_of_line();
      }
      /* Input too short, shortest == 3. E.g. .10 [Used 4 as it includes '\n']*/
      else if (strlen(input) < 4)
         printf("[ERROR]: Input was too short\n");
      /* Perform checking input contents itself */
      else{
         /* Remove newline char */
         input[inlen - 1] = '\0';
         
         /* Allow only numerical input + ensure there is only one delimiter '.'*/
         for (i=0; i<inlen-1; i++){
            digit = input[i] - '0';
            if(input[i] == '.')
               numDelim+=1;
            if(digit < 0 || digit > 9){
               if (input[i] != '.')
                  digitCheck = FALSE;
            }
         }
         
         /* Failed digit check */
         if(digitCheck == FALSE || numDelim == 0)
            printf("[ERROR]: Re-check input, price should be in format [dd.cc]\n");
            
         /* Performing dollars, cents checks */
         if (digitCheck == TRUE && numDelim == 1){          
            /* If no dollar component, assign 0 */
            if(input[0] == '.'){
               dollars = 0;
               dollarFlag = FALSE;
               dollarsDone = TRUE;
            }
            else 
            {  /* If there is dollars component, read from token */
               token = strtok(input, ".");
               if(numberCheck(token) == FALSE || strlen(token) > 2)
                  printf("[ERROR]: Re-enter correct dollars value\n");
               else{ 
                  dollars = strtoulConv(token);
                  dollarsDone = TRUE;
               }
            }
            
            /* Checking cents value - based on whether there was dollar component
             * and only when dollar checking has been done 
             */
            if (dollarsDone == TRUE){
               if(dollarFlag == FALSE)
                  token = strtok(input, ".");
               else
                  token = strtok(NULL, ".");
               /* Perform cents check, ensure integer, length == 2 */
               if(numberCheck(token) == FALSE || strlen(token) != 2)
                  printf("[ERROR]: Re-enter correct cents value\n");
               else{
                  cents = strtoulConv(token);
                  /*Final check: If cents is multiple of 5 */
                  if(cents % 5 == 0)
                     exit = TRUE;
                  else
                     printf("[ERROR]: Cents value must be multiple of 5\n");
               }
            }
         }
      }
   } while (!exit);
   
   price->dollars = dollars;
   price->cents = cents;
      
   return SUCCESS;
}

/* Number range validation */
INPUT intRngVerify(int *intOpt, unsigned len, char *prompt, int min, int max)
{
   BOOLEAN exit = FALSE;
   char input[INTLEN + 2];
   int selection, inlen;
   char *endPtr;
   
   do
   {
      printf("%s", prompt);
      fgets(input, len + 2, stdin);
      inlen = strlen(input);
            
      if (input[inlen - 1] != '\n'){
         printf("[ERROR]: Input was too long\n");
         read_rest_of_line();
      }
      else{
         input[inlen - 1] = '\0';
         selection = (int) strtol(input, &endPtr, 10);
         if (strcmp(endPtr, "") != 0)
            printf("[ERROR]: Input needs to be numeric.\n");
         else if (selection < min || selection > max)
            printf("[ERROR]: Input must be between: [%d - %d]\n", min, max); 
         else
            exit = TRUE;
      }
   } while (!exit);
   
   *intOpt = selection;
   return SUCCESS;
}

/* String input validation */
INPUT strVerify(char *name, unsigned len, char *prompt)
{
   BOOLEAN exit = FALSE;
   char input[len + 2];
   int inlen;
   char * userIn;
   
   do
   {
      printf("%s", prompt);
      userIn = fgets(input, len + 2, stdin);
      inlen = strlen(input);
      
      /* [2] Checking for return to menu entry - [enter] or ctrl-d */
      if (userIn == NULL){
         printf("\n");
         return RTM;
      }
      if (strcmp(userIn, "\n") == 0)
         return RTM;
         
      if (input[inlen - 1] != '\n'){
         printf("[ERROR]: Input was too long.\n");
         read_rest_of_line();
      } 
      else
         exit = TRUE;
   } while (!exit);

   input[inlen - 1] = '\0';
   strcpy(name, input);
   return SUCCESS;
}

/* Validation of user entered cash denominations into the system */
INPUT insertCheck(unsigned * inserted, unsigned len, char *prompt)
{
   BOOLEAN exit = FALSE;
   BOOLEAN digitCheck;
   char *userIn;
   char input[len + 2];
   int i, inlen, digit;
   /* int selection; */
   unsigned result;
   unsigned denoms[NUM_DENOMS] = {5, 10, 20, 50, 100, 200, 500, 1000};
   
   do
   {
      /* Default values for every iteration of loop */
      digitCheck = TRUE;
      
      printf("%s", prompt);
      userIn = fgets(input, len + 2, stdin);
      inlen = strlen(input);

      /* [3] Checking for return to menu entry - [enter] or ctrl-d */
      if (userIn == NULL){
         printf("\n");
         return RTM;
      }
      if (strcmp(userIn, "\n") == 0)
         return RTM;
         
      /* Input too long */
      if (input[inlen - 1] != '\n'){
         printf("[ERROR]: Input was too long\n");
         read_rest_of_line();
      }
      /* Input too short, shortest == 2. E.g. [blank] + '\n' [Used 2 as it includes '\n']*/
      else if (strlen(input) < 2)
         printf("[ERROR]: Input was too short\n");
      /* Perform checking input contents itself */
      else{
         /* Remove newline char */
         input[inlen - 1] = '\0';
         
         /* Allow only numerical input */
         for (i=0; i<inlen-1; i++){
            digit = input[i] - '0';
            if(digit < 0 || digit > 9){
               if (input[i] != '.')
                  digitCheck = FALSE;
            }
         }
         
         /* Failed digit check */
         if(digitCheck == FALSE)
            printf("[ERROR]: Re-check input, price should only contain digits\n");
            
         /* Checking if part of system's denominations */
         if (digitCheck == TRUE){
            /* Convert to unsigned format */
            result = strtoulConv(input);
            /* Perform check against denominations */
            for(i=0; i<NUM_DENOMS; i++){
               if(result == denoms[i]){
                  exit = TRUE;
               }
            }
            if (exit == FALSE)
               printf("[ERROR]: Denomination %d cannot be used with this system\n", result);
         }
      }
   } while (!exit);

   *inserted = result;
   return SUCCESS;
}

/* This function is to convert strings to unsigned. */
unsigned strtoulConv(char * str){
   char *tokenptr;
   unsigned output;
   
   output = strtoul(str, &tokenptr, 10);
   return output;
}

/* This function is used to check whether the input is numerical */
BOOLEAN numberCheck(char * str){
   char *tokenptr;
   unsigned output;
   int pos;
   
   /* Removing unwanted '\n' character(s) */
   for(pos = 0; pos<sizeof(str); pos++){
      if(str[pos] == '\n')
         str[pos] = '\0';
   }
   output = strtoul(str, &tokenptr, 10);
   if (strcmp(tokenptr, "") != 0){
      return FALSE;
   }
   
   return TRUE;
}