/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 * Course Code      : COSC2408
 * Program Code     : BP162B
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/
#include "ppd_coin.h"

 /**
  * @file ppd_coin.c implement functions here for managing coins in the
  * "cash register" contained in the @ref ppd_system struct.
  **/

/* Function to calculate dollars and cents returned based on payment */
void convCurrency(unsigned price, unsigned * dollars, unsigned * cents){
   *dollars = (price / 100);
   *cents = (price % 100);
}

/* Function to obtain dollars to print*/
void getPrice(char * price, unsigned dollars, unsigned cents){
   /* 7 As at most the size is: $dd.cc (6) + '\0' */
   int priceSize = PRICE_LEN + 1;
   
   /* Case 1: d = [0-9], c = [10-95] (E.g. $ 9.50) */
   if(dollars < 10 && cents > 5)
      snprintf(price, priceSize, "$ %d.%d", dollars, cents);

   /* Case 2: d = [10-99], c = [10-95] (E.g. $10.50) */
   if(dollars > 9 && cents > 5)
      snprintf(price, priceSize, "$%d.%d", dollars, cents);

   /* Case 3: d = [0-9], c = [0/5] (E.g. $ 9.05) */
   if(dollars < 10 && cents <= 5)
      snprintf(price, priceSize, "$ %d.0%d", dollars, cents);
      
   /* Case 4: d = [10-99], c = [0/5] (E.g. $ 9.50) */
   if(dollars >= 10 && cents <= 5)
      snprintf(price, priceSize, "$%d.0%d", dollars, cents);
}