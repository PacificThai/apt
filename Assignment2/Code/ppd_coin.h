/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 * Course Code      : COSC2408
 * Program Code     : BP162B
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

/* Extra #include and #define */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* In reference to "6. Display Coins", label for each denomination */
#define LABEL_LEN 10
/* Size of denomination inserted value: Max = 1000 ($10.00), Min = 5 (5c)*/
#define DENOM_LEN 4
/* Length of printed price string itself E.g. $15.00 */
#define PRICE_LEN 6
/* Number of digits for coins in cash register, assumed to be < 100 [max = 99]*/
#define COUNT_LEN 2

/* Enable use of BOOLEAN type */
/* #include "ppd_main.c" */

/**
 * @file ppd_coin.h defines the coin structure for managing currency in the
 * system. You should declare function prototypes for managing coins here
 * and implement them in ppd_coin.c
 **/
#ifndef PPD_COIN
#define PPD_COIN
#define COIN_DELIM ","
struct ppd_system;
/**
 * enumeration representing the various types of currency available in
 * the system. 
 **/
enum denomination
{
   FIVE_CENTS, TEN_CENTS, TWENTY_CENTS, FIFTY_CENTS, ONE_DOLLAR, 
   TWO_DOLLARS, FIVE_DOLLARS, TEN_DOLLARS
};

/**
 * represents a coin type stored in the cash register. Each demonination
 * will have exactly one of these in the cash register.
 **/
struct coin
{
    /**
     * the denomination type
     **/
    enum denomination denom;
    /**
     * the count of how many of these are in the cash register
     **/
    unsigned count;
};
/* Extra functions/code added */
void convCurrency(unsigned price, unsigned * dollars, unsigned * cents);
void getPrice(char * price, unsigned dollars, unsigned cents);
#endif
