/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 * Course Code      : COSC2408
 * Program Code     : BP162B
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_main.h"
#include "ppd_menu.h"
#include "ppd_options.h"
#include "ppd_utility.h"

/**
 * @file ppd_main.c contains the main function implementation and any 
 * helper functions for main such as a display_usage() function.
 **/

/**
 * manages the running of the program, initialises data structures, loads
 * data and handles the processing of options. The bulk of this function
 * should simply be calling other functions to get the job done.
 **/
int main(int argc, char **argv)
{
   int i, choice, min = 1, max = MENU_SIZE;
   BOOLEAN exit = FALSE;
   char prompt[PROMPTLEN + 1];
   struct ppd_system system;
   
   const char *items_file, *coins_file;   
   items_file = argv[1];   coins_file = argv[2];
   
   /* initialise the menu system */
   struct menu_item mainMenu[MENU_SIZE] =
   {
     {"", display_items},  {"", purchase_item},    {"", save_system},
     {"", add_item},       {"", remove_item},      {"", display_coins},
     {"", reset_stock},    {"", reset_coins},      {"", NULL}
   };
      
   init_menu(mainMenu);

   /* validate command line arguments */
   if(argc != NUMARGS){
      fprintf(stderr, "[ERROR]: Not enough arguments, exiting...\n");
      return EXIT_FAILURE;
   }
   
   /* init the system */
   system_init(&system);
   
   if(load_data(&system, coins_file, items_file) == FALSE)
      return EXIT_FAILURE;
   
   do 
   {
      print_menu(mainMenu);
      sprintf(prompt, "Select your option (1-9): ");
      intRngVerify(&choice, INTLEN, prompt, min, max);
      /* Choice -1 as arrays start at 0 */
      choice-=1;
      
      if(choice != 8)
         mainMenu[choice].function(&system);
      if (choice == 2 || choice == 8)
         exit = TRUE;
         
   } while (exit==FALSE);

   system_free(&system);
   return EXIT_SUCCESS;
}