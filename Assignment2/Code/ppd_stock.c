/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 * Course Code      : COSC2408
 * Program Code     : BP162B
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/
#include "ppd_stock.h"
#include "ppd_main.h"

 /**
  * @file ppd_stock.c this is the file where you will implement the 
  * interface functions for managing the stock list.
  **/

/************************************************************************ 
* I would like to acknowledge the addStock() and removeStock() functions 
* contains code in relation to that found on the course notes, 
* Module 9: Linkedlistexample.pdf
* I changed some things around to make it more understandable for myself.
************************************************************************/
  
void addStock(struct ppd_system * system, char * id, char * name, char * desc,
struct price price, unsigned on_hand)
{
   node *previous, *current;
   node *newNode;
   struct ppd_stock *newItem;

   /* Allocating memory to both the node + its data */
   newNode = malloc(sizeof(node));
   newItem = malloc(sizeof(struct ppd_stock));
   
   /* New item data to add to item_list */
   strcpy(newItem->id, id);
   strcpy(newItem->name, name);
   strcpy(newItem->desc, desc);
   newItem->price = price;
   newItem->on_hand = on_hand;
   
   /* Create new node (item), assign data */
   newNode->data = newItem;
   
   /* Set up initially pointing at head */
   previous = NULL;
   current = system->item_list->head;
   
   /* Perform check that not first item & that new data inserted based on 
    * alphabetical order */
   while (current != NULL && strcmp(newNode->data->name, current->data->name) > 0) {
      previous = current;
      current = current->next;
   }

   /* For when first item is inserted into system, otherwise next item */
   if (previous == NULL)
      system->item_list->head = newNode;
   else
      previous->next = newNode;
  
   newNode->next = current;
   system->item_list->count++;
}

node * searchStock(struct ppd_system * system, char * id)
{
   node *previous, *current;
   
   previous = NULL;
   current = system->item_list->head;
   
   while (current != NULL && strcmp(current->data->id, id) != 0) {
      previous = current;
      current = current->next;
   }
         
   /* When node not found, NULL is returned */
   return current;
}

BOOLEAN removeStock(struct ppd_system * system, char * id){
   node *previous, *current;
   
   previous = NULL;
   current = system->item_list->head;
   
   while (current != NULL && strcmp(current->data->id, id) != 0) {
      previous = current;
      current = current->next;
   }
   
   /* When id was not found */ 
   if (current == NULL)
      return FALSE;
   
   /* When the head node is to be deleted, otherwise any other node */
   if (previous == NULL)
      system->item_list->head = current->next;
   else
      previous->next = current->next;
   
   free(current->data);
   free(current);
   system->item_list->count--;
   
   return TRUE;
}

void generateID(struct ppd_system * system, char *newID){
   node *previous, *current;
   char *id, *addedChar;
   char createdID[IDLEN + 1];
   int i, idNum, idLen, maxID=0;
   
   previous = NULL;
   current = system->item_list->head;
   
   while (current != NULL) {
      idNum = 0;
      id = current->data->id;

      /* Generate id number based on index position: 1000s, 100s, 10s, 1 */
      /* id[index] - '0' converts char value into digit */
      idNum += 1000 * (id[1] - '0');
      idNum += 100 * (id[2] - '0');
      idNum += 10 * (id[3] - '0');
      idNum += id[4] - '0';
      
      /* Ensures highest ID number recorded */
      if(maxID < idNum)
         maxID = idNum;

      previous = current;
      current = current->next;
   }
   
   /* Increment to generate new ID value */
   maxID++;
   /* Perform checks to determine length and extra chars that are needed */
   if(maxID < 10)
      addedChar = "I000";
   else if(maxID < 100)
      addedChar = "I00";
   else if(maxID < 1000)
      addedChar = "I0";
   else if(maxID <= 9999)
      addedChar = "I";
   else{
      printf("[ERROR]: Cannot add any more items into the system\n");
      newID = "[ERROR]";
   }
      
   /* Create ID with sprintf (combines: 'I' with digits) */
   sprintf(createdID, "%s%d", addedChar, maxID);
   idLen = strlen(createdID);
   strcpy(newID, createdID);
}

/* Function to check whether its possible to give change */
BOOLEAN changeCheck(struct ppd_system * system, unsigned change){
   int index;
   BOOLEAN changeFlag = FALSE;
   BOOLEAN changePossible = FALSE;
   unsigned denoms[NUM_DENOMS];
   /* Storing original change value to compare with */
   unsigned changeVal = change;
   
   /* For storing copy of system's denominations */
   for (index = 0; index < NUM_DENOMS; index++)
      denoms[index] = system->cash_register[index].denom;
   
   do{
      for (index = NUM_DENOMS - 1; index >= 0; index--){
         if(denoms[index] <= change && system->cash_register[index].count > 0){
            change -= denoms[index];
         }
      }
      /* Exit loop when either known change is possible or after going
       * through register, no denomination could be removed for change */
      if(change == 0 || change == changeVal)
         changeFlag = TRUE;
         
   }while(changeFlag == FALSE);
   
   if(change == 0)
      changePossible = TRUE;
      
   return changePossible;
}