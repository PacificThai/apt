/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 * Course Code      : COSC2408
 * Program Code     : BP162B
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_menu.h"
/**
 * @file ppd_menu.c handles the initialised and management of the menu 
 * array
 **/

/**
 * @param menu the menu item array to initialise
 **/
void init_menu(struct menu_item * menu)
{   
   int menuIndex;
   const char * menuOptions[MENU_SIZE] = 
   {
      "        1. Display Items",
      "        2. Purchase Items",    
      "        3. Save and Exit",
      "        4. Add Item",       
      "        5. Remove Item",
      "        6. Display Coins", 
      "        7. Reset Stock",
      "        8. Reset Coins",       
      "        9. Abort Program"
   };
   
   for(menuIndex=0; menuIndex<MENU_SIZE; menuIndex++){
      strcpy(menu[menuIndex].name, menuOptions[menuIndex]);
   }
}

void print_menu(struct menu_item * menu)
{
   int menuIndex;
   
   printf("--------------------------------------------------------\n");
   printf("Main Menu:\n");
   for(menuIndex=0; menuIndex<MENU_SIZE; menuIndex++){
      printf("%s\n", menu[menuIndex].name);
      if(menuIndex == 3)
         printf("Administrator-Only Menu:\n"); 
   }
   printf("--------------------------------------------------------\n");
}