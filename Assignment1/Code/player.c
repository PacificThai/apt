/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Pacific Thai
* Student Number   : s3429648
* Course Code      : COSC2408
* Program Code     : BP162B
* Start up code provided by Paul Miller
***********************************************************************/
/***********************************************************************
* An acknowledgement would like to be made in relation to generating 
* random numbers for allocating token color to a player and the Computer 
* player selecting a column to drop their token when it is their turn.
* Portions of this code were renamed for clarity and understanding for
* myself, (Code found on: http://codepad.org/zfAyc24z)
***********************************************************************/
#include "player.h"

/**
 * @param human the human player to initialise
 **/
enum input_result get_human_player(struct player * human)
{
   char name[NAMELEN];
   char prompt[PROMPTLEN + 1];
   
   /* Name allocation */
   sprintf(prompt, "Enter your name [Max %d characters]:\n", NAMELEN);
   if(getName(name, NAMELEN, prompt) == RTM){
      printf("Returning to menu...\n");
      return RTM;
   }
   strcpy(human->name, name);
   
   human->counters = 0;
   human->type = HUMAN;
   return SUCCESS;
}

/**
 * @param computer the computer player to initialise
 **/
enum input_result get_computer_player(struct player * computer)
{
   strcpy(computer->name, "Computer");
   computer->counters = 0;
   computer->type = COMPUTER;
   return SUCCESS;
}

/**
 * In this requirement, you need to handle the taking of a turn - either
 * of a human or a computer player.
 *
 * For the human player, you will need to allow the user to enter the 
 * column they wish to place a token in. You will need to validate what 
 * the user enters, then place a token correctly in that column so that
 * it occupies the cell closest to the bottom of the board array for that 
 * column.
 *
 * For the computer player, you will need to randomly generate a column 
 * number to place a token in and if that column is full, generate a 
 * different column number until a column number with some free space has 
 * been found.
 *
 * @param current the current player
 * @param board the game board that we will attempt to insert a token into
 * @return enum @ref input_result indicating the state of user input (in 
 * case this run involved user input
 **/
enum input_result take_turn(struct player * current,
        enum cell_contents board[][BOARDWIDTH])
{
   int colChoice, i;
   char prompt[PROMPTLEN + 1];
   BOOLEAN validDrop = FALSE;
   srand (time(NULL));
      
   /* Computer Player */
   if (current->type == COMPUTER){
      do{
         /* Generate choice: from 1-7 then -1 [array start from 0]*/
         colChoice = (MAXCOLUMN + rand() /
           (RAND_MAX / (MINCOLUMN-2 - MAXCOLUMN + 1) + 1)) - 1;
         /* Valid move testing */      
         if(board[0][colChoice] == 0){
            for(i=BOARDHEIGHT-1; i>=0; i--){
               if(board[i][colChoice] == 0){
                  board[i][colChoice] = current->thiscolor;
                  validDrop = TRUE;
                  break;
               }
            }
         }
      } while (!validDrop);
      /* Show board once computer has made move */
      printf("******** [%s's] turn *******\n", current->name);
      display_board(board);
      printf("[Computer] has dropped a token\n");
   }
   
   /* Human Player */
   if (current->type == HUMAN){
      /* Display board so player knows where to place token */
      printf("******* [%s's] turn *******\n", current->name);
      display_board(board);
      do{
         /* Requesting token drop location (1 to 7), need to -1*/
         sprintf(prompt, "Please enter a column in which to drop a token:\n");
         /* Testing where RTM return from inner option */
         if(getIntOpt(&colChoice, INTLEN, prompt, MINCOLUMN, MAXCOLUMN) == -1){
            return RTM;
         }
         colChoice-=1;
         /* Valid move testing */
         if(board[0][colChoice] == 0){
            for(i=BOARDHEIGHT-1; i>=0; i--){
               if(board[i][colChoice] == 0){
                  board[i][colChoice] = current->thiscolor;
                  validDrop = TRUE;
                  display_board(board);               
                  break;
               }
            }
         }
         if (!validDrop)
            printf("Column is full, try another\n");
      } while (!validDrop);
   }
   
   current->counters+=1;   
   return SUCCESS;
}

/* Extra functions */
/* Allocating player color */
void setColor(struct player * human, struct player * computer){
   int color, min = 1, max = 2;

   srand (time(NULL));
   color = max + rand() / (RAND_MAX / (min-2 - max + 1) + 1);
   
   human->thiscolor = color;
   if(human->thiscolor == C_RED)
      computer->thiscolor = C_WHITE;
   else
      computer->thiscolor = C_RED;
}

/* Validating menu selection (1-3) & Dropping token (1-7) */
int getIntOpt(int *intOpt, unsigned len, char *prompt, int min, int max)
{
   BOOLEAN exit = FALSE;
   char input[INTLEN + 2];
   int selection, inlen;
   char *endPtr;
   char *userIn;
   
   do
   {
      printf("%s", prompt);
      userIn = fgets(input, len + 2, stdin);
      inlen = strlen(input);
      
      /* RTM 1: If user enters [ctrl-d] or [enter], return to menu*/
      if (userIn == NULL)
         return RTM;
      if (strcmp(userIn, "\n") == 0){
         return RTM;
      }
      
      if (input[inlen - 1] != '\n'){
         printf("[ERROR]: Input was too long\n");
         read_rest_of_line();
      }
      else{
         input[inlen - 1] = '\0';
         selection = (int) strtol(input, &endPtr, 10);
         if (strcmp(endPtr, "") != 0){
            printf("[ERROR]: Input needs to be numeric.\n");
         }
         else if (selection < min || selection > max){
            printf("[ERROR]: Input must be between: [%d - %d]\n", min, max); 
         }
         else
            exit = TRUE;
      }
   } while (!exit);
   
   *intOpt = selection;
   return SUCCESS;
}

/* Player Name Validation */
int getName(char *name, unsigned len, char *prompt)
{
   BOOLEAN exit = FALSE;
   char input[NAMELEN + 2];
   int inlen;
   char *userIn;
   
   do
   {
      printf("%s", prompt);
      userIn = fgets(input, len + 2, stdin);
      inlen = strlen(input);
      
      /* RTM 2: If user enters [ctrl-d] or [enter], return to menu*/
      if (userIn == NULL)
         return RTM;
      if (strcmp(userIn, "\n") == 0){
         input[inlen - 1] = '\0';
         return RTM;
      }
      
      if (input[inlen - 1] != '\n'){
         printf("[ERROR]: Input was too long.\n");
         read_rest_of_line();
      } 
      else {
         exit = TRUE;
      }
   } while (!exit);

   input[inlen - 1] = '\0';
   strcpy(name, input);
   return SUCCESS;
}
