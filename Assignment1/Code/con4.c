/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Pacific Thai
* Student Number   : s3429648
* Course Code      : COSC2408
* Program Code     : BP162B
* Start up code provided by Paul Miller
***********************************************************************/
#include "con4.h"

/**
 * @mainpage COSC1076 - Assignment 1 documentation.
 *
 * This is the main html documentation of assignment 1 for COSC1076 -
 * Advanced Programming Techniques for semester 2, 2015. You will find
 * here a description of the functions that you need to implement for
 * your assignment.
 *
 * Please note that this should not be your first stop when starting
 * your assignment. Please read the assignment 1 specifications that
 * are available on blackboard before reading this documentation. The
 * purpose of this documentation is to clarify the actual individual
 * requirements.
 *
 * This site presents you with the documentation for each function
 * that you need to implement broken down by the file that they exist
 * in. Please go to the <b>Files</b> tab and click on the file you wish to
 * get more information about and then click on that file. An
 * explanation of each function implemented in that file will be
 * displayed.
 **/
 
 /**
 * @file con4.c contains the main function which is the entry point into the 
 * application and manages the main memory.
 **/

/**
 * the entry point into the game. You should display the main menu and 
 * respond to user requests.
 *
 * The main menu should look like this: 
 * Welcome to connect 4
 *  \n\--------------------
 * 1. Play Game
 * 2. Display High Scores
 * 3. Quit
 * Please enter your choice:
 *
 * This menu should also be redisplayed when the program returns from 
 * running an option.
 **/
int main(void)
{
   int choice, min = 1, max = 3;
   BOOLEAN exit = FALSE;
   char prompt[PROMPTLEN + 1];
   
   scoreboard scores;
   struct player human_player, computer_player, *winner;
   const score *winnerScore;
      
   init_scoreboard(scores);
      
   system("clear");
   do {
      printMenu();
      sprintf(prompt, "Please enter your choice:\n");
      /* RTM option 1: When requesting menu choice */
      if(getIntOpt(&choice, INTLEN, prompt, min, max) == RTM){
         continue;
      }
      system("clear");

      switch(choice){
         /* Play Game Option */
         case 1:
            /* Player and Computer Initialization */
            printf("********[1]. Play Connect 4 *******\n");
            setColor(&human_player, &computer_player);
            
            /* RTM option 2: When requesting player name */
            if(get_human_player(&human_player) == RTM){
               continue;
            }
            get_computer_player(&computer_player);
         
            system("clear");
            winner = play_game(&human_player, &computer_player);
            /* RTM option 3: During player turn, RTM requested */
            if (strcmp(winner->name, "") == 0){
               continue;
            }
            
            /* After game conclusion */
            if (winner == NULL)
               printf("It was a draw\n");
            else
               printf("The winner is: [%s]\n", winner->name);
               winnerScore = winner;
               add_to_scoreboard(scores, winnerScore);
               
            break;
         /* Display Scoreboard Option */
         case 2:
            display_scores(scores);
            break;
         /* Exit Option */
         default:
            exit = TRUE;
      }
   } while (exit==FALSE);
   
   return SUCCESS;
}