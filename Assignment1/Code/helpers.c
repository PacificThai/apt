/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Pacific Thai
* Student Number   : s3429648
* Course Code      : COSC2408
* Program Code     : BP162B
* Start up code provided by Paul Miller
***********************************************************************/
/************************************************************************ 
* I would like to acknowledge the getIntOpt() function contains code
* in relation to that found on the course notes, file: getInteger-basic.c
* by Steven Burrows. I changed some things around to make it more 
* understandable for myself.
* This is also applicable to getName() as conceptually the code relates
* to another file in the same directory: getString-basic.c
* For the code corresponding to ctrl-d user input, it can be referenced
* back to: 
* http://stackoverflow.com/questions/19228645/fgets-and-dealing-with-ctrld-input
************************************************************************/

#include "helpers.h"

/**
 * @file helpers.c contains various functions that help in the
 * implementation of the program. You should put functions you create
 * here unless they logically belong to another module (such as
 * player, board or game
 **/
 
void read_rest_of_line(void)
{
    int ch;
    while(ch = getc(stdin), ch!=EOF && ch != '\n')
        ; /* gobble each character */
    /* reset the error status of the stream */
    clearerr(stdin);
}

void printMenu(){
   printf("Welcome to connect 4\n");
   printf("--------------------\n");
   printf("1. Play Game\n");
   printf("2. Display High Scores\n");
   printf("3. Quit\n");
}