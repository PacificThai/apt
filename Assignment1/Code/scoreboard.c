/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Pacific Thai
* Student Number   : s3429648
* Course Code      : COSC2408
* Program Code     : BP162B
* Start up code provided by Paul Miller
***********************************************************************/

/************************************************************************ 
* I would like to acknowledge the logic for sorting the scoreboard contains 
* code in relation to that found on the site: 
* http://www.sanfoundry.com/c-program-sort-array-ascending-order/ , 
* variables made to be more understandable for myself
************************************************************************/

#include "scoreboard.h"

/**
 * @param board the scoreboard to initialise
 **/
void init_scoreboard(scoreboard board)
{
   int index;
   
   /* Initialization of empty player */
   struct player empty;
   strcpy(empty.name, " ");
   empty.counters = 0;
   
   /* Adding empty player to each position in array [0 - 9] */
   for (index=0; index<SCOREBOARDSIZE; index++){
      board[index] = empty;
   }
}
/** For this requirement, you will need to display the scores in the
 * order they are scored in the scoreboard array. 
 *
 * The display should look as follows: 
 * Player               |Score
 * --------------------------------------------- 
 * Barney               |17 
 * Magi                 |15 
 * Red                  |10 
 * Computer             |8 
 * Computer             |7 
 * Computer             |6 
 * Paul                 |4 
 * Fred                 |4 
 * Muaddib              |4
 * Zafiqa               |4
 * 
 * @param board the scoreboard to display
 **/
void display_scores(const scoreboard board)
{
   int index;
   printf("########### Connect 4: Scoreboard ###########\n");
   printf("---------------------------------------------\n");
   printf("Player               |Score\n");
   printf("---------------------------------------------\n");
   
   for (index=0; index<SCOREBOARDSIZE; index++){
      /* Displaying non-empty entries */
      /* printf: 21 = occupy 21 spaces, -ive = left aligned */
      if (strcmp(board[index].name, " ") != 0){
         printf("%-21s|%d\n", board[index].name, board[index].counters);
      }
   }
   
   printf("---------------------------------------------\n");
   printf("\nReturning to menu...\n");
}

/** When the game ends, you need to return the appropriate game state
 * back to main. You will then need to insert the winner's score
 * sorted in order by the number of counters that they played in the
 * game. You should only store the top ten scores and so when a new score
 * is entered on a full scoreboard, the lowest score simply drops off the
 * bottom of the board.  
 * 
 * Both scoreboard and score are typedefs (aliases) of other types.
 *
 * Scoreboard is defined as: typedef struct player
 *
 * scoreboard[SCOREBOARDSIZE]; and score is defined as: 
 *
 * typedef struct player score; 
 *
 * In other words, a scoreboard is an array of struct player of
 * SCOREBOARDSIZE (10) and a score is another name of a player struct.
 * This has been done so we can reuse the type and it simplifies the
 * maintenance of the code.
 * 
 * @param board the scoreboard to add the score to @param sc the score
 * to add to the scoreboard
 **/ 
 
 BOOLEAN add_to_scoreboard(scoreboard board, const score * sc) 
 {
   int index, nextIn;
   struct player temp;
   
   /* entry = Player to add to scoreboard */
   struct player entry;
   strcpy(entry.name, sc->name);
   entry.counters = sc->counters;

   /* Adding score to scoreboard */
   for (index = 0; index<SCOREBOARDSIZE; index++){
      /* Check if position is empty, add if so */
      if (board[index].counters == 0){
         board[index] = entry;
         break;
      }
      /* If last entry reached, insert new score */
      if (index == SCOREBOARDSIZE - 1){
         board[index] = entry;
      }
   }
   
   /* Sorting logic */
   for (index = 0; index<SCOREBOARDSIZE; index++){
      for (nextIn = index + 1; nextIn<SCOREBOARDSIZE; nextIn++){
         if (board[index].counters < board[nextIn].counters){
            temp = board[index];
            board[index] = board[nextIn];
            board[nextIn] = temp;
         }
      }   
   }
   
   return TRUE; 
}