/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Pacific Thai
* Student Number   : s3429648
* Course Code      : COSC2408
* Program Code     : BP162B
* Start up code provided by Paul Miller
***********************************************************************/
#include "game.h"

/**
 * @file game.c contains the functions that relate to the management of
 * the game.
 **/

/**
 * This requirement requires you to understand a little more about pointers.
 * We want you to understand that all pointers just point to another memory
 * address whether they are a single pointer, a pointer to a pointer, etc.
 * 
 * In this example, we want you to swap the pointers for two players. 
 * This will be called from \ref play_game with a call such as 
 * swap(&current, &other) and at the end of this function, current and
 * other's pointers will be swapped.
 * For example, if current points to the human player and other points to 
 * the computer player, at the end of this function, the player they point 
 * to will have been swapped. 
 * 
 * @param current the current player who has just completed their turn
 * @param other the next player whose turn it will be
 **/
static void swap_players(struct player ** current, struct player ** other)
{
   struct player temp;   
   temp = **current;
   **current = **other;
   **other = temp;
}

/**
 * This is the heart of the game.
 * 
 * Firstly, you need to initialise the game by calling functions written 
 * for requirement 3. The rest of this function is then managing the game 
 * loop.
 *
 * Each player takes a turn to drop a token into a column on the game 
 * board. The task that needs to be performed will be different depending 
 * on whether this is a human or a computer player.
 *
 * Regardless of the player type, you will need to display the board as it 
 * was before the player makes their move. If the current player is a 
 * computer player, we just need to select a column at random and drop a
 * token into that column. If there is no room, try another column.
 *
 * If the current player is a human player, your program will need to ask 
 * the user what column they wish to drop a token in. This input should be 
 * validated to check that it is numeric and within the range of allowed 
 * columns.
 * 
 * Once a valid move has been entered, your program should update the 
 * gameboard. It should then check if the game has been won lost or drawn 
 * by calling and the change the current player.
 *
 * This function also checks if the game has been won, lost or drawn by 
 * calling \ref test_for_winner(), and if the game is over, returns the 
 * winner to main or NULL if the game was a draw.

 * @param human a pointer to details about the human player
 * @param computer a pointer to details about the computer player
 **/
struct player * play_game(struct player * human , struct player* computer)
{  
   enum cell_contents board[BOARDHEIGHT][BOARDWIDTH];   
   struct player *current, *other;
   enum game_state state;
   
   /* Dummy player added, returned with RTM requested */
   struct player dummy;
   struct player *rtm;
   strcpy(dummy.name, "");
   rtm = &dummy;
   
   state = G_NO_WINNER;
   
   /* Determine who goes first based on token color, notify */
   printf("##### Token allocation: #####\n");
   if(human->thiscolor == C_WHITE){
      printf("[%s's] token: %s\n", human->name, WHITE_TOKEN);
      printf("[%s's] token: %s\n", computer->name, RED_TOKEN);
      current = human;
      other = computer;
   } else {
      printf("[%s's] token: %s\n", human->name, RED_TOKEN);
      printf("[%s's] token: %s\n", computer->name, WHITE_TOKEN);
      current = computer;
      other = human;
   }
   printf("[%s] goes first\n\n", current->name);
   
   /* Board Setup */   
   initialise_board(board);

   /* Game loop*/
   do{ 
      if(take_turn(current, board) == RTM){
         printf("[%s] has quit the game\n", current->name);
         return rtm;
      }
      swap_players(&current, &other);
      state = test_for_winner(board);
   } while (state == G_NO_WINNER);
   
   switch (state){
      case G_RED:
         if (human->thiscolor == C_RED){
            return human;
         } else {
            return computer;
         }
      case G_WHITE:
         if (human->thiscolor == C_WHITE){
            return human;
         } else {
            return computer;
         }
      default:
         return NULL;
   }
}

/**
 * In this requirement you are required to test what the game's current 
 * state is.
 * Possible game states are defined by the game_state enumeration:
 * enum game_state
 * {
 *    G_NO_WINNER=-1,
 *    G_DRAW,
 *    G_RED,
 *    G_WHITE
 * };
 *
 * Most of these states should be self-explanatory but let's go through 
 * their meaning.
 * 
 *    &bull; G_NO_WINNER: the game is still in progress and therefore there
 *    is no winner yet.
 * 
 *    &bull; G_DRAW: neither player has won the game but there are no more 
 *    spaces left for a player to make a move.
 *    
 *    &bull; G_RED / G_WHITE: the player whose token is the specified 
 *    colour has won the game.
 *
 * Your task in this function is to iterate over the 2-dimensional array 
 * (the board) looking for four in a row in any direction – if you find 
 * this, return the appropriate value of either G_RED or G_WHITE.
 *
 * Next, test for a draw. If none of these cases hold, return G_NO_WINNER.
 * @param board the gameboard to test for a winner
 **/
enum game_state test_for_winner(enum cell_contents board[][BOARDWIDTH])
{    
   int row, col, x, y;    
   int whiteInRow, redInRow, total = 0;
   
   /* Note: BOARDHEIGHT - 1 as we need to start from index 5 not 6 
          : BOARDWIDTH - 1 as well, from index 6 not 7 */
   
   /* Checking for 4 in a row, horizontally */
   for (row=BOARDHEIGHT-1; row>=0; row--){
      for (col=0; col<=3; col++){
         whiteInRow = 0;
         redInRow = 0;
         for (y=col; y<col+NUM_IN_ROW; y++){
            switch(board[row][y]){
               case C_RED:
                  redInRow++;
                  continue;
               case C_WHITE:
                  whiteInRow++;
                  continue;
               case C_EMPTY:
                  break;
            }
         }
         if (redInRow == NUM_IN_ROW)
            return G_RED;
         if (whiteInRow == NUM_IN_ROW)
            return G_WHITE;
      }
   }
   
   /* Check for 4 in a row, vertically */
   for (col=0; col<BOARDWIDTH; col++){
      for (row=BOARDHEIGHT-1; row>=3; row--){
         whiteInRow = 0;
         redInRow = 0;
         for (x=row; x>row-NUM_IN_ROW; x--){
            switch(board[x][col]){
               case C_RED:
                  redInRow++;
                  continue;
               case C_WHITE:
                  whiteInRow++;
                  continue;
               case C_EMPTY:
                  break;
            }           
         }
         if (redInRow == NUM_IN_ROW)
            return G_RED;
         if (whiteInRow == NUM_IN_ROW)
            return G_WHITE;
      }      
   }     
   
   /* Check for 4 in a row, diagonally */
      /* 1. Check Left->Right: ['/'] shape */
   for (row=BOARDHEIGHT-1; row>=3; row--){
      for (col=0; col<=3; col++){
         whiteInRow = 0;
         redInRow = 0;
         y = col;
         for (x=row; x>row-NUM_IN_ROW; x--){
            switch(board[x][y]){
               case C_RED:
                  redInRow++;
                  break;
               case C_WHITE:
                  whiteInRow++;
                  break;
               case C_EMPTY:
                  break;
            }
            y+=1;
         }
         if (redInRow == NUM_IN_ROW)
            return G_RED;
         if (whiteInRow == NUM_IN_ROW)
            return G_WHITE;
      }
   }
   
      /* 2. Check Right->Left: ['\'] shape */
   for (row=BOARDHEIGHT-1; row>=3; row--){
      for (col=BOARDWIDTH-1; col>=3; col--){
         whiteInRow = 0;
         redInRow = 0;
         y=col;
         for (x=row; x>row-NUM_IN_ROW; x--){
            switch(board[x][y]){
               case C_RED:
                  redInRow++;
                  break;
               case C_WHITE:
                  whiteInRow++;
                  break;
               case C_EMPTY:
                  break;
            }
            y-=1;            
         }
         if (redInRow == NUM_IN_ROW)
            return G_RED;
         if (whiteInRow == NUM_IN_ROW)
            return G_WHITE;  
      }
   }
  
   /* Check for all cells filled, draw */
   for (row=0; row < BOARDHEIGHT; row++){
      for (col=0; col < BOARDWIDTH; col++){
         if(board[row][col] == C_RED || board[row][col] == C_WHITE)
            total++;
         else
            break;
      }
      if (total == 42)
         return G_DRAW;
   }
   
   /* If none of above return Winner/Draw outcome, continue game */
   return G_NO_WINNER;
}

