/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Pacific Thai
* Student Number   : s3429648
* Course Code      : COSC2408
* Program Code     : BP162B
* Start up code provided by Paul Miller
***********************************************************************/
#include "board.h"
#include "player.h"

/**
 * @file board.c contains implementations of functions related to the game 
 * board.
 **/

/**
 * @param board the board to reset the contents of
 **/
void initialise_board(enum cell_contents board[][BOARDWIDTH])
{
   int row, col;
   for (row=0; row < BOARDHEIGHT; row++){
      for (col=0; col < BOARDWIDTH; col++){
         board[row][col] = C_EMPTY;
      }
   }
}

/**
 * In this requirement you are required to display the game board. 
 * The game board should be displayed as shown on the first page of the 
 * assignment specification. 
 * @param board the board to display
 **/
void display_board(enum cell_contents board[][BOARDWIDTH])
{
   int row, col;
   char *content;
   printf("This is the current state of the board:\n\n");
   printf(" 1 | 2 | 3 | 4 | 5 | 6 | 7 |\n");
   printf("----------------------------\n");
   for (row=0; row < BOARDHEIGHT; row++){
      for (col=0; col < BOARDWIDTH; col++){
         /* Switch: analyze content for each cell, determine token */
         switch(board[row][col]){
            case C_RED:
               content = RED_TOKEN;
               break;
            case C_WHITE:
               content = WHITE_TOKEN;
               break;
            default:
               content = " ";
         }
         printf(" %s |", content);
      }
      printf("\n");
      for (col=0; col < BOARDWIDTH; col++){
         printf("----");
      }
      printf("\n");
   }
}
